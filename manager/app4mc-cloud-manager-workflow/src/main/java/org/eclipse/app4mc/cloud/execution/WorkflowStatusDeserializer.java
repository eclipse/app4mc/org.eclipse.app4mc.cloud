/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * Custom {@link StdDeserializer} for {@link WorkflowStatus} objects, that
 * resolves the selected {@link CloudServiceDefinition}s and the corresponding
 * {@link ServiceConfiguration} by name/key.
 */
public class WorkflowStatusDeserializer extends StdDeserializer<WorkflowStatus> {
	
	private static final long serialVersionUID = -272147724534718705L;
	
	private List<CloudServiceDefinition> cloudServiceDefinitions;
	private boolean loadServiceDetails = true;
	
    public WorkflowStatusDeserializer(List<CloudServiceDefinition> cloudServiceDefinitions) { 
        this(null, cloudServiceDefinitions, true); 
    } 
    
    public WorkflowStatusDeserializer(List<CloudServiceDefinition> cloudServiceDefinitions, boolean loadServiceDetails) { 
    	this(null, cloudServiceDefinitions, loadServiceDetails); 
    } 
    
    public WorkflowStatusDeserializer(Class<?> vc, List<CloudServiceDefinition> cloudServiceDefinitions, boolean loadServiceDetails) { 
    	super(vc);
    	this.cloudServiceDefinitions = cloudServiceDefinitions;
    	this.loadServiceDetails = loadServiceDetails;
    }

    @Override
    public WorkflowStatus deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        WorkflowStatus result = new WorkflowStatus();
        JsonNode node = jp.getCodec().readTree(jp);
        
        JsonNode name = node.get("name");
        if (name != null) {
        	result.setName(name.asText());
        }
        JsonNode uuid = node.get("uuid");
        if (uuid != null) {
        	result.setUuid(uuid.asText());
        }

        // only load the service details if needed
        // can be disabled for loading a list of executed workflows
        if (this.loadServiceDetails) {
        	JsonNode services = node.get("services");
        	if (services != null) {
        		services.fields().forEachRemaining(serviceField -> {
        			deserializeServiceNode(result, null, serviceField.getKey(), serviceField.getValue().fields());
        		});
        	}
        }
    	
        JsonNode cancelled = node.get("cancelled");
        if (cancelled != null && cancelled.asBoolean()) {
        	result.cancel();
        }
        JsonNode done = node.get("done");
        if (done != null && done.asBoolean()) {
        	result.done();
        }

        JsonNode messages = node.get("messages");
        if (messages != null) {
        	messages.elements().forEachRemaining(n -> result.addMessage(n.asText()));
        }
        
        JsonNode errors = node.get("errors");
        if (errors != null) {
        	errors.elements().forEachRemaining(n -> result.addError(n.asText()));
        }
        
        JsonNode results = node.get("results");
        if (results != null) {
        	results.fields().forEachRemaining(field -> result.addResult(field.getKey(), field.getValue().asText()));
        }
        
    	return result;
    }
    
    private void deserializeServiceNode(
    		WorkflowStatus result, 
    		String parentKey, 
    		String serviceKey, 
    		Iterator<Map.Entry<String, JsonNode>> fields) {
    	
        CloudServiceDefinition csd = this.cloudServiceDefinitions.stream()
				.filter(sd -> sd.getKey().equals(serviceKey))
				.findFirst()
				.orElse(null);
        if (csd != null) {
        	// resolve the service configuration
        	ServiceConfiguration config = WorkflowStatusHelper.getConfigurationForService(csd);
        	
        	// add the service to the workflow status so that the recursive calls work for nested service nodes
        	if (parentKey == null) {
        		result.addSelectedService(csd, config);
        	} else {
        		result.addSelectedService(parentKey, csd, config);
        	}

        	fields.forEachRemaining(configField -> {
    			if (!configField.getValue().isContainerNode()) {
    				// service parameter are simple values, no containers
    				ServiceConfigurationParameter param = config.getParameter(configField.getKey());
    				if (param != null) {
    					param.setValue(configField.getValue().asText());
    				}
    			} else {
    				// resolve nested services
    				deserializeServiceNode(
    						result, 
    						parentKey != null ? parentKey + "." + serviceKey : serviceKey, 
    						configField.getKey(), 
    						configField.getValue().fields());
    			}
    		});
        } else {
        	// there is no service definition for the given key, so it is probably a structural node
        	String nodeKey = parentKey != null ? parentKey + "." + serviceKey : serviceKey;
        			
			fields.forEachRemaining(serviceField -> {
        		deserializeServiceNode(result, nodeKey, serviceField.getKey(), serviceField.getValue().fields());
        	});
        }
    }
}
