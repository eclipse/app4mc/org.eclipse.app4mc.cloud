/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

/**
 * The message model class to transport a processing log message from server to the client.
 */
public class ProcessLog {

	public enum Action {
		UPDATE, DONE, PROCESSING
	}
	
	private Action action;
	private String message;
	private String uuid;

	public ProcessLog() {
	}
	
	public ProcessLog(Action action) {
		this(action, null);
	}
	
	public ProcessLog(Action action, String message) {
		this(action, message, null);
	}
	
	public ProcessLog(Action action, String message, String uuid) {
		this.action = action;
		this.message = message;
		this.uuid = uuid;
	}

	public Action getAction() {
		return action;
	}
	
	public void setAction(Action action) {
		this.action = action;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getUuid() {
		return this.uuid;
	}
}
