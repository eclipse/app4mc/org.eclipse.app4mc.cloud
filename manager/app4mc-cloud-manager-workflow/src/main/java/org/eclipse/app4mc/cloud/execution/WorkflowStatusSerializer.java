/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * Custom {@link StdSerializer} for {@link WorkflowStatus} objects, that writes
 * only the names of the selected {@link CloudServiceDefinition}s and writes the
 * corresponding {@link ServiceConfiguration} directly below the service.
 */
public class WorkflowStatusSerializer extends StdSerializer<WorkflowStatus> {

	private static final long serialVersionUID = 8920512086262532108L;

	public WorkflowStatusSerializer() {
        super(WorkflowStatus.class);
    }
    
	@Override
	public void serialize(WorkflowStatus value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeStartObject();
		
		if (value.getName() != null) {
			gen.writeStringField("name", value.getName());
		}
		
		if (value.getUuid() != null) {
			gen.writeStringField("uuid", value.getUuid());
		}
		
		gen.writeFieldName("services");
		gen.writeStartObject();
		for (ServiceNode node : value.getSelectedServices()) {
			serializeServiceNode(node, gen);
		}
		
		gen.writeEndObject();
		
		// if the uuid is empty, it is likely that it was not executed and not done,
		// therefore we do not need to serialize those states
		if (value.getUuid() != null) {
			gen.writeBooleanField("cancelled", value.isCancelled());
			gen.writeBooleanField("done", value.isDone());
		}
		
		if (value.hasMessages()) {
			gen.writeObjectField("messages", value.getMessages());
		}
		if (value.hasErrors()) {
			gen.writeObjectField("errors", value.getErrors());
		}
		if (value.hasResults()) {
			gen.writeObjectField("results", value.getResults());
		}
		gen.writeEndObject();
	}
	
	private void serializeServiceNode(ServiceNode node, JsonGenerator gen) throws IOException {
		gen.writeFieldName(node.getId());
		gen.writeStartObject();

		if (!node.isStructuralNode()) {
			ServiceConfiguration configuration = node.getServiceConfiguration();
			if (configuration != null) {
				configuration.getParameterList().stream()
					.sorted((o1, o2) -> o1.getKey().compareTo(o2.getKey()))
					.filter(p -> p.getValue() != null)
					.forEach(param -> {
						try {
							gen.writeObjectField(param.getKey(), param.getValue());
						} catch (IOException e) {
							throw new RuntimeException(e);
						}
					});
			}
		}
		
		if (!node.getChildren().isEmpty()) {
			for (ServiceNode child : node.getChildren()) {
				serializeServiceNode(child, gen);
			}
		}
		
		gen.writeEndObject();
	}
}
