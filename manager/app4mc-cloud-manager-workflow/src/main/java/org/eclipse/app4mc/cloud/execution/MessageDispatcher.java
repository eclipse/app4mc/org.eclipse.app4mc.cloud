/*********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

/**
 * Interface for implementing a dispatcher that handles messages sent by the
 * cloud service execution. For the manager web application, the implementation
 * wraps the SimpMessageSendingOperations. This way a dependency from the
 * execution library to a websocket implementation can be avoided.
 */
public interface MessageDispatcher {

	/**
	 * Send a message to the given destination.
	 * @param destination the target destination
	 * @param payload the payload to send
	 */
	void send(String destination, Object payload);

}
