/*********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * The service configuration definition that is provided by a service.
 */
@JsonDeserialize(using = ServiceConfigurationDefinitionDeserializer.class)
public class ServiceConfigurationDefinition {

	/**
	 * The description of the service to explain what the service does.
	 */
	private String description;
	/**
	 * The file type that is accepted as input, e.g. amxmi, json, btf.
	 */
	private String inputType;
	/**
	 * The accepted input version, for the Amalthea Model e.g. 0.9.9 or 1.0.0.
	 */
	private String inputVersion;
	/**
	 * Whether the service supports input provided as archive.
	 */
	private boolean inputArchiveSupported = false;
	
	/**
	 * The file type that is produced as a result, e.g. amxmi, json, btf.
	 */
	private String outputType;
	/**
	 * The produced output version, for the Amalthea Model e.g. 0.9.9 or 1.0.0.
	 */
	private String outputVersion;
	/**
	 * Whether the service supports producing the output as archive.
	 */
	private boolean outputArchiveSupported = false;

	/**
	 * The configuration parameter the service supports.
	 */
	private ArrayList<ServiceConfigurationParameter> parameter = new ArrayList<>();
	
	public ServiceConfigurationDefinition() {
		// empty constructor needed for JSON serialization 
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	public String getInputVersion() {
		return inputVersion;
	}

	public void setInputVersion(String inputVersion) {
		this.inputVersion = inputVersion;
	}

	public boolean isInputArchiveSupported() {
		return this.inputArchiveSupported;
	}
	
	public void setInputArchiveSupported(boolean supported) {
		this.inputArchiveSupported = supported;
	}

	public String getOutputType() {
		return outputType;
	}

	public void setOutputType(String outputType) {
		this.outputType = outputType;
	}

	public String getOutputVersion() {
		return outputVersion;
	}

	public void setOutputVersion(String outputVersion) {
		this.outputVersion = outputVersion;
	}

	public boolean isOutputArchiveSupported() {
		return this.outputArchiveSupported;
	}
	
	public void setOutputArchiveSupported(boolean supported) {
		this.outputArchiveSupported = supported;
	}
	
	public void addParameter(ServiceConfigurationParameter param) {
		this.parameter.add(param);
	}
	
	public List<ServiceConfigurationParameter> getParameterList() {
		ArrayList<ServiceConfigurationParameter> params = new ArrayList<>(this.parameter);
		params.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
		return params;
	}
	
	public ServiceConfigurationParameter getParameter(String key) {
		for (ServiceConfigurationParameter param : this.parameter) {
			if (param.getKey().equals(key)) {
				return param;
			}
		}
		return null;
	}
}
