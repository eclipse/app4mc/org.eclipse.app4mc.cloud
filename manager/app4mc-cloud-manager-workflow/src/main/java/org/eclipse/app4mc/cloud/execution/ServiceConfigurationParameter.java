/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import java.util.ArrayList;
import java.util.List;

public class ServiceConfigurationParameter {

	private String name;
	private String description;
	private String key;
	private String value;
	private String type = "String";
	private String cardinality = "single";
	private boolean mandatory = false;
	private List<String> possibleValues = new ArrayList<>();
	private boolean managerParameter = false;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getCardinality() {
		return cardinality;
	}
	
	public void setCardinality(String cardinality) {
		this.cardinality = cardinality;
	}
	
	public boolean isMandatory() {
		return mandatory;
	}
	
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public List<String> getPossibleValues() {
		return possibleValues;
	}

	public void setPossibleValues(List<String> possibleValues) {
		this.possibleValues = possibleValues;
	}

	public boolean isManagerParameter() {
		return managerParameter;
	}

	public void setManagerParameter(boolean managerParameter) {
		this.managerParameter = managerParameter;
	}
	
}
