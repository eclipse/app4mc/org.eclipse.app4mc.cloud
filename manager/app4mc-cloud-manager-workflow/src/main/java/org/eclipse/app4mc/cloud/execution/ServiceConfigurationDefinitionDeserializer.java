/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * Custom {@link StdDeserializer} for {@link ServiceConfigurationDefinition} objects.
 */
public class ServiceConfigurationDefinitionDeserializer extends StdDeserializer<ServiceConfigurationDefinition> {
	
	private static final long serialVersionUID = 857606567523680918L;
	
	protected ServiceConfigurationDefinitionDeserializer() {
		super(ServiceConfigurationDefinition.class);
	}

    @Override
    public ServiceConfigurationDefinition deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
    	ServiceConfigurationDefinition result = new ServiceConfigurationDefinition();
        JsonNode node = jp.getCodec().readTree(jp);
        
        JsonNode description = node.get("description");
        if (description != null) {
        	result.setDescription(description.asText());
        }
        
        JsonNode input = node.get("input");
        if (input != null) {
        	JsonNode typeNode = input.get("type");
        	if (typeNode != null) {
        		result.setInputType(typeNode.asText());
        	}
        	JsonNode versionNode = input.get("version");
        	if (versionNode != null) {
        		result.setInputVersion(versionNode.asText());
        	}
        	JsonNode archiveNode = input.get("archive-supported");
        	if (archiveNode != null) {
        		result.setInputArchiveSupported(archiveNode.asBoolean());
        	}
        }
        
        JsonNode output = node.get("output");
        if (output != null) {
        	JsonNode typeNode = output.get("type");
        	if (typeNode != null) {
        		result.setOutputType(typeNode.asText());
        	}
        	JsonNode versionNode = output.get("version");
        	if (versionNode != null) {
        		result.setOutputVersion(versionNode.asText());
        	}
        	JsonNode archiveNode = output.get("archive-supported");
        	if (archiveNode != null) {
        		result.setOutputArchiveSupported(archiveNode.asBoolean());
        	}
        }

        JsonNode parameter = node.get("parameter");
        if (parameter != null) {
        	parameter.fields().forEachRemaining(parameterField -> {
        		deserializeParameterNode(result, parameterField.getKey(), parameterField.getValue());
        	});
        }
        
    	return result;
    }
    
    private void deserializeParameterNode(
    		ServiceConfigurationDefinition result, 
    		String parameterKey, 
    		JsonNode fields) {
    	
    	ServiceConfigurationParameter param = new ServiceConfigurationParameter();
    	param.setKey(parameterKey);
    	
    	JsonNode name = fields.get("name");
    	if (name != null) {
    		param.setName(name.asText());
    	}
    	
    	JsonNode description = fields.get("description");
    	if (description != null) {
    		param.setDescription(description.asText());
    	}
    	
    	JsonNode type = fields.get("type");
    	if (type != null) {
    		param.setType(type.asText());
    	}
    	
    	JsonNode value = fields.get("value");
    	if (value != null) {
    		param.setValue(value.asText());
    	}
    	
    	JsonNode cardinality = fields.get("cardinality");
    	if (cardinality != null) {
    		param.setCardinality(cardinality.asText());
    	}
    	
    	JsonNode mandatory = fields.get("mandatory");
    	if (mandatory != null) {
    		param.setMandatory(mandatory.asBoolean());
    	}
    	
    	JsonNode values = fields.get("values");
    	if (values != null) {
    		if (values.isArray()) {
    			ArrayList<String> valueList = new ArrayList<>();
    			values.forEach(v -> valueList.add(v.asText()));
    			param.setPossibleValues(valueList);
    		} else {
    			param.setPossibleValues(Arrays.asList(values.asText()));
    		}
    	}
    	
    	result.addParameter(param);
    }
}
