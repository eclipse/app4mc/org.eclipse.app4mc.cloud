/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.manager.administration;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.eclipse.app4mc.cloud.execution.CloudServiceDefinition;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kong.unirest.Proxy;
import kong.unirest.Unirest;

@Controller
public class AdminController {

	@Resource(name = "cloudServiceDefinitions")
	List<CloudServiceDefinition> cloudServiceDefinitions;
	
	@GetMapping("/admin")
    public String adminRequest(final Model model) {
		CloudServiceDefinitionDTO dto = new CloudServiceDefinitionDTO(cloudServiceDefinitions);
		dto.addService(new CloudServiceDefinition());
		
		model.addAttribute("dto", dto);
		
		Proxy proxy = Unirest.config().getProxy();
		model.addAttribute("proxy_host", proxy != null ? proxy.getHost() : "");
		model.addAttribute("proxy_port", proxy != null ? proxy.getPort() : "");
		model.addAttribute("proxy_user", proxy != null ? proxy.getUsername() : "");
		model.addAttribute("proxy_pwd", proxy != null ? proxy.getPassword() : "");
		
        return "admin";
    }
	
	@PostMapping("/admin/save")
	public String saveServices(@ModelAttribute CloudServiceDefinitionDTO dto, Model model) {
		// TODO save to DB
		cloudServiceDefinitions.clear();
		cloudServiceDefinitions.addAll(dto.getServices().stream()
				.filter(csd -> !StringUtils.isEmpty(csd.getKey()) && !StringUtils.isEmpty(csd.getName()) && !StringUtils.isEmpty(csd.getBaseUrl()))
				.collect(Collectors.toList()));
	 
	    model.addAttribute("dto", new CloudServiceDefinitionDTO(cloudServiceDefinitions));
	    return "redirect:/admin";
	}
	
	@PostMapping("/admin/saveProxy")
	public String saveProxy(Model model,
			@RequestParam(name = "proxy_host", required = false) String proxyHost, 
			@RequestParam(name = "proxy_port", required = false) String proxyPort, 
			@RequestParam(name = "proxy_user", required = false) String proxyUser, 
			@RequestParam(name = "proxy_pwd", required = false) String proxyPwd) {
		
		// TODO save to DB and load from DB in App4McCloudManagerApplication
		Unirest.config().reset();
        if (!StringUtils.isEmpty(proxyHost) && !StringUtils.isEmpty(proxyPort)) {
       		Unirest.config().socketTimeout(0).proxy(proxyHost, Integer.valueOf(proxyPort), proxyUser, proxyPwd);
        } else {
        	Unirest.config().socketTimeout(0).proxy(null);
        }
        
		return "redirect:/admin";
	}
	
	@PostMapping("/admin/remove/{selected}")
	public String removeService(
			@PathVariable(name = "selected") String selected,
			@ModelAttribute CloudServiceDefinitionDTO dto) {
		
		for (Iterator<CloudServiceDefinition> it = cloudServiceDefinitions.iterator(); it.hasNext();) {
			CloudServiceDefinition csd = it.next();
			if (selected.equals(csd.getKey())) {
				it.remove();
			}
		}
		
		return "redirect:/admin";
	}

	@ModelAttribute("cloudServiceDefinitions")
	public List<CloudServiceDefinition> cloudServiceDefinitions() {
		return cloudServiceDefinitions;
	}
}
