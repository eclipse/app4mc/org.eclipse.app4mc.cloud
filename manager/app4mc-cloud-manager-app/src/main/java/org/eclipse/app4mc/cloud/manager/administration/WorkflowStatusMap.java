/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.manager.administration;

import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.app4mc.cloud.execution.WorkflowStatus;

public class WorkflowStatusMap {

	private ConcurrentHashMap<String, WorkflowStatus> workflowStatusMap = new ConcurrentHashMap<>();
	
	public void put(String key, WorkflowStatus status) {
		this.workflowStatusMap.put(key, status);
	}
	
	public WorkflowStatus get(String key) {
		return this.workflowStatusMap.get(key);
	}
	
	public void clear() {
		this.workflowStatusMap.clear();
	}
}
