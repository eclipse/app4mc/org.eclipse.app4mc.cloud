/*********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.amlt2systemc;

import org.eclipse.app4mc.transformation.ServiceConstants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * EventHandler for the CM_DELETED event that is fired once the session
 * configuration is deleted when the transformation is finished.
 */
@Component(property = EventConstants.EVENT_TOPIC + "=org/osgi/service/cm/ConfigurationEvent/CM_DELETED")
public class SessionConfigHandler implements EventHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(SessionConfigHandler.class);

    @Reference
    private SessionRegistry registry;
    
	@Override
	public void handleEvent(Event event) {
    	if (event.getProperty("cm.factoryPid").equals(ServiceConstants.SESSION_CONFIGURATION_PID)) {
    		String pid = event.getProperty("cm.pid").toString();
    		String uuid = pid.substring(pid.indexOf('~') + 1);
    		LOGGER.debug("Finish transformation session with id {}", uuid);
    		if (!registry.isError(uuid)) {
    			registry.setFinished(uuid);
    		}
    	}
	}

}
