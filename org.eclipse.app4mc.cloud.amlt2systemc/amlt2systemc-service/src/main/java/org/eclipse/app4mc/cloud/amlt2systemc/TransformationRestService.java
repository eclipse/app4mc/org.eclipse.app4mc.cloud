/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.amlt2systemc;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.transformation.ServiceConstants;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.transformation.TransformationProcessor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JSONRequired;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Component(service=TransformationRestService.class, scope = ServiceScope.PROTOTYPE)
@JaxrsResource
@Produces(MediaType.APPLICATION_JSON)
@JSONRequired
@Path("app4mc/amlt2systemc")
public class TransformationRestService {

	private static final String TEMP_DIR_PREFIX = "app4mc_amlt2systemc_";

	private static final Logger LOGGER = LoggerFactory.getLogger(TransformationRestService.class);
	
	private static final String ERROR_FILE = "error.txt";

	private static final String PARENT_OUTPUT_FOLDER = "output";
	private static final String TRANSFORMER_ID = "APP4MCSIM";
	
	private final String defaultBaseDir = System.getProperty("java.io.tmpdir");
	
	private ExecutorService executor = Executors.newFixedThreadPool(1);
	
    @Reference
    private TransformationProcessor processor;
    
    @Reference
    private SessionRegistry registry;
    
	@GET
	@Path("config")
	public String config() {
		ObjectMapper mapper = new ObjectMapper();
		
		ObjectNode config = mapper.createObjectNode();
		config.put("description", "Transform an Amalthea model to simulation code.");
		
		ObjectNode input = mapper.createObjectNode();
		input.put("type", "amxmi");
		input.put("version", ModelUtil.MODEL_VERSION);
		input.put("archive-supported", true);
		config.set("input", input);

		ObjectNode output = mapper.createObjectNode();
		output.put("type", "app4mc-sim-model");
		output.put("archive-supported", true);
		config.set("output", output);

		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(config);
		} catch (JsonProcessingException e) {
			return "Error in generating configuration definition: " + e.getMessage();
		}
	}
	
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response upload(@Context HttpServletRequest request, @Context UriInfo uriInfo) throws IOException, ServletException {
    	
    	Link self = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()).rel("self").build();

    	Part part = request.getPart("file");
    	if (part != null && part.getSubmittedFileName() != null && part.getSubmittedFileName().length() > 0) {
    		String filename = part.getSubmittedFileName();
    		try (InputStream is = part.getInputStream()) {
    			java.nio.file.Path tempFolderPath = Files.createTempDirectory(TEMP_DIR_PREFIX);
    			
    			// extract uuid from pathname
    			String uuid = tempFolderPath.toString().substring(tempFolderPath.toString().lastIndexOf('_') + 1);
    			
    			// copy file to temporary location
    			java.nio.file.Path uploaded = Paths.get(tempFolderPath.toString(), filename);
    			Files.copy(is, uploaded);
    			
    			if (Files.exists(uploaded)) {
    				// mark uuid in progress
    				this.registry.setInProgress(uuid);
    				
    				// trigger asynchronous processing
    				executor.execute(() -> {
    					
    					try {
    						Properties properties = new Properties();
    						properties.put(TransformationConstants.INPUT_MODELS_FOLDER, tempFolderPath.toString());
    						properties.put(TransformationConstants.OUTPUT_FOLDER, Paths.get(tempFolderPath.toString(), PARENT_OUTPUT_FOLDER));
    						properties.put(TransformationConstants.M2T_TRANSFORMERS, TRANSFORMER_ID);
    						properties.put(ServiceConstants.SESSION_ID, uuid);
    						
    						processor.startTransformation(properties);
    					} catch (Exception e) {
    						LOGGER.error("Failure in transformation", e);
    		    			this.registry.setError(uuid);
    						error(tempFolderPath, "Failure in transformation", e);
    					}
    				});
    				
    				// file upload succeeded and processing is triggered
    				Link statusLink = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()
                            .path(uuid))
    						.rel("status")
    						.build();
    				
    				return Response
    						.created(statusLink.getUri())
    						.entity(uuid)
    						.links(self, statusLink)
    						.build();
    			} else {
    				// file upload failed
    				return Response
    						.status(Status.NOT_FOUND)
    						.entity("Model file upload failed!")
    						.links(self)
    						.build();
    			}
    		}
    	}

    	return Response
    			.status(Status.BAD_REQUEST)
    			.entity("No model file provided!")
    			.links(self)
    			.build();
    }
    
    @Path("{uuid}")
    @GET
    public Response status(@PathParam("uuid") String uuid, @Context UriInfo uriInfo) throws IOException {
    	CacheControl cacheControl = new CacheControl();
    	cacheControl.setNoCache(true);
    	cacheControl.setNoStore(true);
    	
    	Link self = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()).rel("self").build();
    	
    	java.nio.file.Path tempFolderPath = Paths.get(defaultBaseDir, TEMP_DIR_PREFIX + uuid);
    	if (!Files.exists(tempFolderPath)) {
			return Response
					.status(Status.NOT_FOUND)
					.entity("No status resource available for id " + uuid)
					.build();
    	}
    	
    	boolean hasErrorFile = false;
    	try (Stream<java.nio.file.Path> files = Files.list(tempFolderPath)) {
    		hasErrorFile = files.anyMatch(path -> path.endsWith(ERROR_FILE));
    	}
    	
    	if (this.registry.isInProgress(uuid)) {
    		return Response
    				.accepted()
    				.links(self)
    				.cacheControl(cacheControl)
    				.build();
    	} else if (this.registry.isError(uuid) || hasErrorFile) {
        	// processing finished with error
    		Link errorLink = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()
                    .path("error"))
    				.rel("error")
    				.build();

    		return Response
    				.noContent()
    				.links(self, errorLink)
    				.cacheControl(cacheControl)
    				.build();
    	}
    	
    	// processing is finished
		Link downloadLink = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()
                .path("download"))
				.rel("result")
				.build();

		return Response
				.ok()
				.links(self, downloadLink)
				.cacheControl(cacheControl)
				.build();
    }

    @Path("{uuid}/download")
    @GET
    @Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON } )
    public Response download(@PathParam("uuid") String uuid, @Context UriInfo uriInfo) throws IOException {
    	Link self = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()).rel("self").build();

    	java.nio.file.Path tempFolderPath = Paths.get(defaultBaseDir, TEMP_DIR_PREFIX + uuid);
    	if (!Files.exists(tempFolderPath)) {
			return Response
					.status(Status.NOT_FOUND)
					.entity("No download resource available for id " + uuid)
					.build();
    	}

    	// if process is in progress, the download resource is 404
    	if (this.registry.isInProgress(uuid)) {
    		return Response
    				.status(Status.NOT_FOUND)
    				.entity("Process is still in progresss")
    				.links(self)
    				.build();
    	}

    	java.nio.file.Path path = Paths.get(tempFolderPath.toString(), PARENT_OUTPUT_FOLDER, "m2t_output_text_files", TRANSFORMER_ID, "result.zip");
    	if (!Files.exists(path)) {
			return Response
					.status(Status.NOT_FOUND)
					.entity("No transformation result available!")
    				.links(self)
					.build();
		}
    	
		List<PathSegment> pathSegments = uriInfo.getPathSegments();
    	UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder().replacePath("");
    	for (int i = 0; i < pathSegments.size() - 1; i++) {
    		uriBuilder.path(pathSegments.get(i).getPath());
    	}
		Link deleteLink = Link.fromUriBuilder(uriBuilder)
				.rel("delete")
				.build();

		return Response
				.ok()
				.entity(path.toFile())
				.header("Content-Disposition", "attachment; filename=\"" + path.toFile().getName() + "\"")
				.links(self, deleteLink)
				.build();
    }
    
    @Path("{uuid}/error")
    @GET
    @Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON } )
    public Response error(@PathParam("uuid") String uuid, @Context UriInfo uriInfo) throws IOException {
    	Link self = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()).rel("self").build();

    	java.nio.file.Path tempFolderPath = Paths.get(defaultBaseDir, TEMP_DIR_PREFIX + uuid);
    	
    	if (!Files.exists(tempFolderPath)) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
    	
    	boolean hasErrorFile = false;
    	try (Stream<java.nio.file.Path> files = Files.list(tempFolderPath)) {
    		hasErrorFile = files.anyMatch(path -> path.endsWith(ERROR_FILE));
    	}

    	// if there is no error file, the error resource is 404
    	if (!hasErrorFile) {
    		return Response
    				.status(Status.NOT_FOUND)
    				.entity("No error occured")
    				.links(self)
    				.build();
    	}
    	
    	java.nio.file.Path errorFilePath = Paths.get(tempFolderPath.toString(), ERROR_FILE);
    	
		List<PathSegment> pathSegments = uriInfo.getPathSegments();
    	UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder().replacePath("");
    	for (int i = 0; i < pathSegments.size() - 1; i++) {
    		uriBuilder.path(pathSegments.get(i).getPath());
    	}
		Link deleteLink = Link.fromUriBuilder(uriBuilder)
				.rel("delete")
				.build();

		return Response
				.ok(errorFilePath.toFile())
				.header("Content-Disposition", "attachment; filename=\"" + errorFilePath.toFile().getName() + "\"")
				.links(self, deleteLink)
				.build();
    }
    
    @Path("{uuid}")
    @DELETE
    public Response delete(@PathParam("uuid") String uuid) throws IOException {
    	java.nio.file.Path path = Paths.get(defaultBaseDir, TEMP_DIR_PREFIX + uuid);
    	
    	if (!Files.exists(path)) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
    	
    	Files.walk(path)
	    	.sorted(Comparator.reverseOrder())
	    	.map(java.nio.file.Path::toFile)
	    	.forEach(File::delete);
    	
    	this.registry.removeStatus(uuid);
    	
    	return Response.ok().build();
    }


	private void error(java.nio.file.Path resultFolder, String message, Exception exception) {
		try {
			java.nio.file.Path errorFilePath = Files.createFile(Paths.get(resultFolder.toString(), ERROR_FILE));
			try (PrintWriter writer = new PrintWriter(Files.newOutputStream(errorFilePath))) {
				writer.append(message).append(System.lineSeparator());
				if (exception != null) {
					exception.printStackTrace(writer);
				}
			}
			
		} catch (IOException e) {
			LOGGER.error("Failed to write error.txt", e);
		}
	}
}
